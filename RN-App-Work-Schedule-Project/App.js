import React, { useState, useEffect } from 'react';
import axios from 'axios'
import {
  Picker,
  Alert,
  ImageBackground,
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';


import Login from './components/login'
import ForgotPassword from './components/forgotpassword'
import Home from './components/home'
import Profile from './components/profile'
import Schedule from './components/schedule'
import Jobtask from './components/jobtask'
import Contact from './components/contact'
import SearchJobs from './components/search'


export default function App() {


  // ============ Employee Login =============


  let [userInfo, setUserInfo] = useState({})
  let [isLoggedIn, setIsLoggedIn] = useState(false)


  //function that change user content when loggedin and loggetout
  // useEffect when load page check if there is a token in localstorage
  // if token== null setUser to false 
  // : send token to server '/users/verify_token'
  // if ok : true change setUser to true else set to false 

  useEffect(() => {
    // AsyncStorage.removeItem('asyncUserInfo')
    checkStorage()
  }, [])
  const checkStorage = async () => {
    try {
      const asyncUserInfo = await AsyncStorage.getItem('asyncUserInfo')
      const token = JSON.parse(asyncUserInfo)

      asyncUserInfo !== null
        ? changeContent(token)
        : setIsLoggedIn(false)
    } catch (error) {
      console.log(error)
    }
  }

  const changeContent = async (token) => {
    // debugger
    try {
      const response = await axios.post(`http://165.22.195.215/server/employees/verify_token`, { token })

      console.log("response in changeContent", response)
      return response.data.ok
      //  ? 
      //  (setIsLoggedIn(true), setUserInfo({
      //              ...userInfo,
      //              ...response.data.userData
      //             //also put books list in the state
      //           }) 
      //   ): setIsLoggedIn(false)
    }
    catch (error) {
      console.log(error)
    }
  }

  // ====== Login/Logout ======
  const login = async (token, user) => {
    //debugger
    try {

      await AsyncStorage.setItem('token', JSON.stringify(token))

      setUserInfo({
        ...userInfo,
        ...user
      })
      setIsLoggedIn(true)
    } catch (error) {
      console.log('error =>', error)
    }
  }

  const logout = async () => {


    await AsyncStorage.removeItem('token');
    setIsLoggedIn(false)
    setPage('Login')

  }

  // ============== Type of Employee ============= //

  let [isFreelancer, setIsFreelancer] = useState(false)
  let [isAdmin, setIsAdmin] = useState(false)

  // const logout = () => {
  //   props.logout
  // }
  useEffect(() => {
    (userInfo.employee_type === "Freelancer")
      ? setIsFreelancer(true)
      : setIsFreelancer(false);


    (userInfo.employee_type === "Admin")
      ? setIsAdmin(true)
      : setIsAdmin(false)

  }, [])

  // ============== Functions =====================//
  const [page, setPage] = useState('Login')


  useEffect(() => {
    console.log('page', page)
  }, [page])

  const handlePageChange = (e) => {
    setPage(e)
  }


  // ========================================
  // ============ Rendered Pages ============
  // ========================================



  // ========================
  // ====== Login Page ======
  // ========================

  if (page === 'Login') {
    return (<Login
      handlePageChange={handlePageChange}
      login={login}
      userInfo={userInfo}
    />
    )
  }


  // =================================
  // ====== ForgotPassword Page ======
  // =================================

  if (page === 'ForgotPassword') {
    return (<ForgotPassword
      handlePageChange={handlePageChange}
      userInfo={userInfo}

    />
    )
  }

  // ========================
  // ====== Home Page ======
  // ========================
  if (page === 'Home') {
    return (<Home
      handlePageChange={handlePageChange}
      userInfo={userInfo}

    />
    )
  }

  // ========================
  // ====== Schedule Page ======
  // ========================
  if (page === 'Schedule') {
    return (<Schedule
      handlePageChange={handlePageChange}
      userInfo={userInfo}

    />
    )
  }

  // ========================
  // ====== Jobtask Page ======
  // ========================
  if (page === 'Jobtask') {
    return (<Jobtask
      handlePageChange={handlePageChange}
      userInfo={userInfo}

    />
    )
  }
  // ========================
  // ====== Profile Page ======
  // ========================
  if (page === 'Profile') {
    return (<Profile
      handlePageChange={handlePageChange}
      userInfo={userInfo}
      logout={logout}
    />
    )
  }

  // ========================
  // ====== Contact Page ======
  // ========================
  if (page === 'Contact') {
    return (<Contact
      handlePageChange={handlePageChange}
      userInfo={userInfo}

    />
    )
  }

  // ========================
  // ====== SearchJobs Page ======
  // ========================
  if (page === 'SearchJobs') {
    return (<SearchJobs
      handlePageChange={handlePageChange}
      userInfo={userInfo}

    />
    )
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    // marginTop: 20,
    // justifyContent: 'center',
  },

  text: {
    color: '#000',
  },
  // ====================
  // ====== Header ======
  // ====================

  header: {
    height: '20%',
    width: '100%',
    backgroundColor: '#82D0F0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'red',
  },


  // ====================
  // ====== Login ======
  // ====================

  Login: {
    backgroundColor: '#E0E1E2',
    height: '80%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
  },
  loginBox: {
    height: '20%',
  },
  input: {
    backgroundColor: 'white',
    height: '50%',
    borderWidth: 1,
    borderColor: 'darkgray',
    margin: 2,
  },
  loginButton: {
    width: '60%',
  },


  buttomSections: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },

  ForgotButton: {

  },
  ContactButton: {

  },


  // ====================
  // ====== Login ======
  // ====================

  main: {
    backgroundColor: '#E0E1E2',
    height: '70%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  HomeForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
  },
  footer: {
    height: '10%',
    width: '100%',
    backgroundColor: '#82D0F0',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',

  },
  icon: {
    height: 50,
    width: 50,
    backgroundColor: '#82D0F0',
    color: 'black',
  }
});


