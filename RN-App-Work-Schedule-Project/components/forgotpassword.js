import React, { useState, useEffect } from 'react';
import { Picker, 
        Alert, 
        ImageBackground, 
        Image, 
        StyleSheet, 
        Text, 
        View, 
        TextInput, 
        ScrollView, 
        TouchableOpacity, 
        AsyncStorage} from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';




const ForgotPassword = (props) => {


    return (
            <View style={styles.container}>
            
              {/* ====== Header ====== */}
                <View style={styles.header}>
                  <Text style={styles.headerText}>Forgot your password ?</Text>
        
                </View>
                <View style={styles.main}>
                
              {/* ====== Home Form ====== */}
                  <View style={styles.ForgotPasswordForm}>

                    <View>
                        <Text>Write your email here</Text>
                        <TextInput ></TextInput>
                        <Button>Reset my password</Button>
                        <Text>An email with a reset-link will be sent to your account</Text>
                    </View>
      
               </View>
               <Button 
                      style={styles.gobackButton}
                      raised={true}
                      title={'Go Back'} 
                      type={'outline'}
                      onPress={ () => props.handlePageChange('Login') }/>
        
              </View>
        
              
            </View>
            
            )


}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      // marginTop: 20,
      // justifyContent: 'center',
    },
    
    text: {
      color: '#000',
    },
  // ====================
  // ====== Header ======
  // ====================

  header: {
    height: '20%',
    width: '100%',
    backgroundColor: '#82D0F0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'red',
  },


  // ====================
  // ====== Login ======
  // ====================

  Login: {
    backgroundColor: '#E0E1E2',
    height: '80%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
  },
  loginBox: {
    height: '20%',
  },
  input: {
    backgroundColor: 'white',
    height: '50%',
    borderWidth: 1,
    borderColor: 'darkgray',
    margin: 2,
  },
  loginButton: {
    width: '60%',
  },
  
  
  buttomSections:{
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  
  ForgotButton: {
    
  },
  ContactButton: {
    
  },
  
  
  // ====================
  // ====== Login ======
  // ====================

  main: {
    backgroundColor: '#E0E1E2',
    height: '80%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  HomeForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
  },
  footer:{
    height: '10%',
    width: '100%',
    backgroundColor: '#82D0F0',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
     
  },
  icon: {
    height: 50,
    width: 50,
    backgroundColor: '#82D0F0',
    color: 'black',
  }
});


export default ForgotPassword