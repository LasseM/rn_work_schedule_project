import React, { useState, useEffect } from 'react';
import {
  Picker,
  Alert,
  ImageBackground,
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';
import axios from 'axios'


const Login = (props) => {

  // console.log("props from Emplogin =>", props, [])


  const [emailForm, setEmailForm] = useState({
    employee_email: '',
  })

  const handleEmailChange = e => {
    setEmailForm({ employee_email: e })
  }

  const [passwordForm, setPasswordForm] = useState({
    employee_password: '',
  })


  const handlePasswordChange = e => {
    setPasswordForm({ employee_password: e })
  }
  const handleSubmit = async (e) => {

    let welcomeMsg = []


    try {

      const response = await axios.post(`http://165.22.195.215/server/employees/login`, {
        employee_email: emailForm.employee_email,
        employee_password: passwordForm.employee_password
      })
      welcomeMsg.push(response.data.message)
      welcomeMsg.push('' + response.data.user.employee_firstname)
      welcomeMsg.push('' + response.data.user.employee_lastname)


      // `${response.data.message} ${response.user.employee_firstname} ${response.user.employee_lastname}`
      alert(welcomeMsg.join(' '))
      console.log("response.data =>", response.data)
      // console.log(message)
      if (response.data.ok) {

        setTimeout(() => {
          props.login(response.data.token, response.data.user)
          props.handlePageChange('Home')

        }, 2000)
      }

    }
    catch (error) {

      console.log(error)
    }
  }

  return (
    <View style={styles.container}>

      {/* ====== Header ====== */}
      <View style={styles.header}>
        <Text style={styles.headerText}>Welcome to</Text>
        <Text style={styles.headerText}>DMC Company's Employee App</Text>
      </View>
      <View style={styles.Login}>

        {/* ====== Login Form ====== */}
        <View style={styles.loginForm}>

          <View style={styles.loginBox}>

            <View>
              <Text style={styles.inputlabels}>User Email:</Text>
              <TextInput
                // value= {todo} 
                onChangeText={text => handleEmailChange(text)}
                style={styles.input}
                type='email'
                name='employee_email'
              // name='employee_email' 
              />
            </View>
            <View>
              <Text style={styles.inputlabels}>Password:</Text>
              <TextInput
                // value= {todo} 
                onChangeText={text => handlePasswordChange(text)}
                style={styles.input}
                type="password"
                name='employee_password'
              // name='employee_password' 
              />
            </View>
            <Button
              buttonStyle={styles.Button}
              raised={true}
              title={'login'}
              type={'outline'}
              onPress={handleSubmit} />
          </View>
        </View>
        {/* ====== Forgot / contact DMC Buttons ====== */}
        <View style={styles.buttomSections}>
          <Button
            buttonStyle={styles.Button}
            raised={true}
            title={'Forgot Password'}
            type={'outline'}
            onPress={() => props.handlePageChange('ForgotPassword')} />
          <Button
            buttonStyle={styles.Button}
            raised={true}
            title={'Contact DMC'}
            type={'outline'}
            onPress={() => props.handlePageChange('Contact')} />

        </View>


      </View>



    </View>

  )

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    // marginTop: 20,
    // justifyContent: 'center',
  },

  text: {
    color: '#000',
  },
  // ====================
  // ====== Header ======
  // ====================

  header: {
    height: '20%',
    width: '100%',
    backgroundColor: '#82D0F0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'red',
  },


  // ====================
  // ====== Login ======
  // ====================

  Login: {
    backgroundColor: '#E0E1E2',
    height: '80%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '70%',
    justifyContent: 'flex-start',
    marginBottom: 20,
  },
  loginBox: {
    height: '80%',
    justifyContent: 'space-evenly',
  },
  input: {
    backgroundColor: 'white',
    height: '30%',
    borderWidth: 1,
    borderColor: 'darkgray',
    margin: 5,
    borderRadius: 75,
    paddingLeft: 15,
  },
  inputlabels: {
    marginLeft: 10,
    fontWeight: 'bold',
    color: '#15739A',
  },
  Button: {
    borderColor: 'darkgray',
    borderRadius: 75,

  },


  buttomSections: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%'
  },


 

});


export default Login