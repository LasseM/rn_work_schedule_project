import React, { useState, useEffect } from 'react';
import {
  Picker,
  Alert,
  ImageBackground,
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';
import Axios from 'axios'





const Profile = (props) => {

  console.log('props in profile=>', props)
  let [logoutAlert, setLogoutAlert] = useState(false)

  const showAlert = () => {
    Alert.alert(
      'Alert Title',
      'My Alert Msg',
      [
        { text: 'Are you sure you want to logout?', onPress: () => setLogoutAlert(true) },
        { text: 'Cancel logout', onPress: () => setLogoutAlert(false) }
      ],
      { cancelable: true }
    )
      logoutAlert
      ? props.logout()
      : null
  }
  return (
    <View style={styles.container}>

      {/* ====== Header ====== */}
      <View style={styles.header}>
        <Text style={styles.headerText}>My Profile</Text>

      </View>
      <View style={styles.main}>

        {/* ====== Home Form ====== */}
        <View style={styles.ProfileForm}>

          {/* <Image style={{ width: 200, height: 200, borderRadius:100 }} value="HomeIcon" source={require('../assets/Profile.jpg')} /> */}

          <Text>{props.userInfo.employee_firstname}</Text>
          <Image source={props.userInfo.employee_image.photo_url}/>
          <Text>{props.userInfo.employee_firstname}</Text>
          <Text>{props.userInfo.employee_middlename}</Text>
          <Text>{props.userInfo.employee_lastname}</Text>
          <Text>{props.userInfo.employee_email}</Text>
          <Text>{props.userInfo.employee_addressStreet}</Text>
          <Text>{props.userInfo.employee_addressNumber}</Text>
          <Text>{props.userInfo.employee_postalCode}</Text>
          <Text>{props.userInfo.employee_city}</Text>
          <Text>{props.userInfo.employee_country}</Text>
          <Text>{props.userInfo.employee_phone}</Text>
          <Text>{props.userInfo.employee_mobile}</Text>
          <Text>{props.userInfo.employee_office}</Text>
          <Text>{props.userInfo.employee_position}</Text>
          <Text>{props.userInfo.employee_focusarea}</Text>
          <Text>{props.userInfo.employee_nationality}</Text>
          <Text>{props.userInfo.employee_language}</Text>
          <Text>{props.userInfo.employee_bank_reg}</Text>
          <Text>{props.userInfo.employee_bank_account_number}</Text>
          <Text>{props.userInfo.employee_payroll_num}</Text>
          <Text>{props.userInfo.employee_tax_card}</Text>





          <Button raised={true} 
                  style={styles.button}
                  type={'outline'} 
                  buttonStyle={{ height: 40, width: 120, backgroundColor:'#82D0F0', borderRadius:10 }} 
                  onPress={() => showAlert()}
                  title='Logout'
                  color='darkblue'
                  ></Button>
        </View>


      </View>

      {/* ====== Footer ====== */}
      <View style={styles.footer}>
        {/* <Image style={{width: 50, height: 50}} source={require('./assets/beach_hut_waves_island.jpg')}/> */}
        <TouchableOpacity onPress={() => props.handlePageChange('Home')}>
          <Image style={{ width: 50, height: 50 }} value="HomeIcon" source={require('../assets/homeIcon.png')} />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => props.handlePageChange('Schedule')}>
          <Image style={{ width: 50, height: 50 }} value="ClockIcon" source={require('../assets/clockIcon4.png')} />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => props.handlePageChange('SearchJobs')}>
          <Image style={{ width: 50, height: 50 }} value="SearchIcon" source={require('../assets/searchIcon.png')} />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => props.handlePageChange('Profile')}>
          <Image style={{ width: 50, height: 50 }} value="UserIcon" source={require('../assets/userIcon2.png')} />
        </TouchableOpacity>
      </View>

    </View>

  )


}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    // marginTop: 20,
    // justifyContent: 'center',
  },

  text: {
    color: '#000',
  },
  // ====================
  // ====== Header ======
  // ====================

  header: {
    height: '20%',
    width: '100%',
    backgroundColor: '#82D0F0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'red',
  },


  // ====================
  // ====== Profile ======
  // ====================

 
  ProfileForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  loginBox: {
    height: '20%',
  },
  input: {
    backgroundColor: 'white',
    height: '50%',
    borderWidth: 1,
    borderColor: 'darkgray',
    margin: 2,
  },
  
  Button: {
    width: '60%',
    color: 'black',
  },


  buttomSections: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },

  ForgotButton: {

  },
  ContactButton: {

  },


  // ====================
  // ====== Login ======
  // ====================

  main: {
    backgroundColor: '#E0E1E2',
    height: '70%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  HomeForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
  },
  footer: {
    height: '10%',
    width: '100%',
    backgroundColor: '#82D0F0',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',

  },
  icon: {
    height: 50,
    width: 50,
    backgroundColor: '#82D0F0',
    color: 'black',
  }
});


export default Profile