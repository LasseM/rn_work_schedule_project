import React, { useState } from 'react'

import axios from 'axios'

import {
    Picker,
    Alert,
    ImageBackground,
    Image,
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';

import { Button, ThemeProvider } from 'react-native-elements';



import Dropdown from './dropdownPicker'



const EventFind = () => {


    //==== Get full year from inputfield
    const [inputfullYear, setinputFullYear] = useState('')
    const handleYearChange = (e) => {
        setinputFullYear(e)
    }
    console.log('inputfullYear =>', inputfullYear)

    const [form, setForm] = useState({})
    //========Find event
    const [findEvent, setFindEvent] = useState([])


    const handleFormChange = (e) => {
        const temp = { event_type: e }
        console.log('e in handleFormChange in FindEvent =>', e)
        setForm(temp)
    }
    console.log('form before try in FindEvents =>', form)

    //====== HandleSubmit ====== 
    const handleSubmit = async () => {
        //   e.preventDefault()

        try {
            const response = await axios.get(`http://165.22.195.215/server/events/find_events`)


            // debugger
            const temp = []

            console.log("response ====>:", response)

            response.data.response.forEach((ele) => {
                let tempDate = ''
                let tempDateMonth = ''

                console.log("ele. event dates ", ele.event_date)
                tempDate = ele.event_date
                tempDateMonth = new Date(ele.event_date)
                console.log("tempDateMonth:", tempDateMonth)

                let tempYear = tempDate.slice(0, 4)
                console.log("tempYear:", tempYear)


                let tempMonth = tempDateMonth.getMonth()
                console.log("tempMonth:", tempMonth)

                console.log("inputfullYear:", inputfullYear)


                if (ele.event_type === form.event_type && tempYear === inputfullYear) {
                    temp.push(ele)
                } else if (form.event_type === 'All Event Types' && tempYear === inputfullYear) {
                    temp.push(ele)
                }
            });
            setFindEvent([...temp])
            console.log('temp:', temp)
        } catch (error) {
            // console.log("Error from Server=>", response)
            // console.log('req', req)
            // res.send({ok: false, message:'Something went wrong'})

            // Error 
            if (error.response) {
                /*
                 * The request was made and the server responded with a
                 * status code that falls out of the range of 2xx
                 */
                console.log('error.response.data', error.response.data);
                console.log('error.response.status', error.response.status);
                console.log('error.response.headers', error.response.headers);
            } else if (error.request) {
                /*
                 * The request was made but no response was received, `error.request`
                 * is an instance of XMLHttpRequest in the browser and an instance
                 * of http.ClientRequest in Node.js
                 */
                console.log('error.request', error.request);
            } else {
                // Something happened in setting up the request and triggered an Error
                console.log('Error1', error.message);
            }
            console.log('error2', error);
        }


    }
    return (
        <View>
            <Text>Here you can search for all events</Text>
            <View style={styles.submit} onSubmit={handleSubmit}>



                <View>
                    <Text>*Choose Year:</Text>
                    <TextInput

                        onChangeText={text => handleYearChange(text)}

                        //   onChange={handleYearChange} 
                        type="string"
                        name="chooseYear"
                        style={styles.input}
                    />

                </View>

                <Dropdown handleFormChange={handleFormChange} />

                <Button
                    buttonStyle={{ height: 40 }}
                    title={'search for events'}
                    onPress={() => handleSubmit(form)}
                    raised={true}>Search</Button>





            </View>

            {/* ======= Return of the search ====== */}

            {
                findEvent.map((ele, idx) => {
                    // debugger
                    return <View key={idx} >
                        <Text>Search results for {form.event_type}:</Text>
                        <View style={styles.returnFrom}>
                            <ScrollView>
                                {/* <Text>Number of search results:</Text> <Text>{idx + 1} of {findEvent.length}</Text> */}
                                <Text>event_firstname:</Text>
                                <TextInput
                                    value={ele.event_firstname}
                                    readOnly="true"
                                />
                                <Text>systemID:</Text>
                                <TextInput
                                    value={ele._id}
                                    readOnly="true"
                                />
                                <Text>eventID:</Text>
                                <TextInput
                                    value={ele.eventID}
                                    readOnly="true"
                                />
                                <Text>event_type:</Text>
                                <TextInput
                                    value={ele.event_type}
                                    readOnly="true"
                                />
                                <Text>event_image:</Text>
                                <TextInput
                                    value={ele.event_image}
                                    readOnly="true"
                                />
                                <Text>event_logo:</Text>
                                <TextInput
                                    value={ele.event_logo}
                                    readOnly="true"
                                />
                                <Text>event_middlename:</Text>
                                <TextInput
                                    value={ele.event_middlename}
                                    readOnly="true"
                                />
                                <Text>event_lastname:</Text>
                                <TextInput
                                    value={ele.event_lastname}
                                    readOnly="true"
                                />
                                <Text>event_date:</Text>
                                <TextInput
                                    value={ele.event_date}
                                    readOnly="true"
                                />
                                <Text>event_startdate:</Text>
                                <TextInput
                                    value={ele.event_startdate}
                                    readOnly="true"
                                />
                                <Text>event_starttime:</Text>
                                <TextInput
                                    value={ele.event_starttime}
                                    readOnly="true"
                                />
                                <Text>event_enddate:</Text>
                                <TextInput
                                    value={ele.event_enddate}
                                    readOnly="true"
                                />
                                <Text>event_endtime:</Text>
                                <TextInput
                                    value={ele.event_endtime}
                                    readOnly="true"
                                />
                                <Text>event_country_location:</Text>
                                <TextInput
                                    value={ele.event_country_location}
                                    readOnly="true"
                                />
                                <Text>event_city_location:</Text>
                                <TextInput
                                    value={ele.event_city_location}
                                    readOnly="true"
                                />
                                <Text>event_location:</Text>
                                <TextInput
                                    value={ele.event_location}
                                    readOnly="true"
                                />
                                <Text>event_adress_street:</Text>
                                <TextInput
                                    value={ele.event_adress_street}
                                    readOnly="true"
                                />
                                <Text>event_adress_number:</Text>
                                <TextInput
                                    value={ele.event_adress_number}
                                    readOnly="true"
                                />
                                <Text>event_adress_postcode:</Text>
                                <TextInput
                                    value={ele.event_adress_postcode}
                                    readOnly="true"
                                />
                                <Text>event_adress_country:</Text>
                                <TextInput
                                    value={ele.event_adress_country}
                                    readOnly="true"
                                />
                                <Text>event_staffneeded:</Text>
                                <TextInput
                                    value={ele.event_staffneeded}
                                    readOnly="true"
                                />
                                <Text>event_jobtasks:</Text>
                                <TextInput

                                    value={ele.event_staffneeded}
                                    // ele.event_jobtasks
                                    readOnly="true"
                                />
                                <Text>event_type_of_staff_needed:</Text>
                                <TextInput
                                    value={ele.event_type_of_staff_needed}
                                    readOnly="true"
                                />
                                <Text>event_staffsignedup:</Text>
                                <TextInput
                                    value={ele.event_staffsignedup}
                                    readOnly="true"
                                />
                                <Text>event_language:</Text>
                                <TextInput
                                    value={ele.event_language}
                                    readOnly="true"
                                />
                                <Text>event_general_notes_to_staff:</Text>
                                <TextInput
                                    value={ele.event_general_notes_to_staff}
                                    readOnly="true"
                                />
                                <Text>event_dutymanagers:</Text>
                                <TextInput
                                    value={ele.event_dutymanagers}
                                    readOnly="true"
                                />
                            </ScrollView>
                        </View>
                    </View>
                })
            }

            {/* ================== */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        // marginTop: 20,
        // justifyContent: 'center',
    },

    text: {
        color: '#000',
    },
    // ====================
    // ====== Header ======
    // ====================

    header: {
        height: '20%',
        width: '100%',
        backgroundColor: '#82D0F0',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        color: 'red',
    },


    // ====================
    // ====== Login ======
    // ====================

    main: {
        backgroundColor: '#E0E1E2',
        height: '70%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    submit: {
        height: 'auto',
        backgroundColor: 'purple',
    },
    returnFrom: {
        backgroundColor: 'lightblue',
        width: '80%',
        height: '80%',
        justifyContent: 'space-evenly',
    },
    footer: {
        height: '10%',
        width: '100%',
        backgroundColor: '#82D0F0',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',

    },
    icon: {
        height: 50,
        width: 50,
        backgroundColor: '#82D0F0',
        color: 'black',
    }
});

export default EventFind