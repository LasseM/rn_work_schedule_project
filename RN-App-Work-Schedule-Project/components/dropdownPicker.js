
import React, { useState, useEffect } from 'react'
import RNPickerSelect from 'react-native-picker-select';
import { View } from 'react-native';
 
const Dropdown = (props) => {

    //const [inputValue, setinputValue] = useState('')

    // useEffect((e)=>{
    //     setForm(e)
    // }, [value])
    //() => props.handleFormChange(form),
    //setForm(value),
    //onPress={ () => props.handleFormChange(form) }
    //console.log('value from RNPicker =>', value)
    return (
        <View> 
        <RNPickerSelect
          placeholder={{label:'Select Event Type', value:'null'}}
            onValueChange={ (value) => props.handleFormChange(value) }
            items={[
                { name:'event_type', label: 'All Event Types', value: 'All Event Types' },
                { name:'event_type', label: 'MICE', value: 'MICE' },
                { name:'event_type', label: 'Cruise', value: 'Cruise' },
                
            ]}
        >
    </RNPickerSelect>

                
                </View>
    );
};

export default Dropdown

// <View>
//           <Text>*Choose Event Type:</Text>   

//           <Picker 
//         //   selectedValue={''}
//                   onValueChange={handleFormChange()  =>{
//                   setForm('Value')}}>
                  
//                   <Picker.Item Value="selectType" 
//                                label='Select Event Type'
//                                itemPosition='0'
//                   />
//                   <Picker.Item Value="All" 
//                                label='All Event Types'
//                                itemPosition='1'
//                   />
//                   <Picker.Item Value='MICE' 
//                                label='MICE'
//                                itemPosition='2'
//                   />
//                   <Picker.Item Value='Cruise' 
//                                label='Cruise'
//                                itemPosition='3'
//                   />
//           </Picker>
//         </View>