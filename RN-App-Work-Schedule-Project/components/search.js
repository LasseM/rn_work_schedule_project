import React, { useState, useEffect } from 'react';
import { Picker, 
        Alert, 
        ImageBackground, 
        Image, 
        StyleSheet, 
        Text, 
        View, 
        TextInput, 
        ScrollView, 
        TouchableOpacity, 
        AsyncStorage} from 'react-native';
import { Button, ThemeProvider } from 'react-native-elements';

import { url } from '../config'

import EventFind from './findEvents'

const SearchJobs = (props) => {


    return (
            <View style={styles.container}>
            
              {/* ====== Header ====== */}
                <View style={styles.header}>
                  <Text style={styles.headerText}>Search Jobs</Text>
        
                </View>
                <View style={styles.main}>
                
              {/* ====== Home Form ====== */}
                  {/* <View style={styles.SearchJobsForm}>
            
                  <Image style={{width: 250, height: 200}} value="HomeIcon"source={require('../assets/Profile.jpg')}/>
              
                  
      
               </View> */}
               {
                <EventFind/>
               }
              </View>
        
            {/* ====== Footer ====== */}
              <View style={styles.footer}>
                {/* <Image style={{width: 50, height: 50}} source={require('./assets/beach_hut_waves_island.jpg')}/> */}
                <TouchableOpacity onPress = { () => props.handlePageChange('Home')}>
                <Image style={{width: 50, height: 50}} value="HomeIcon"source={require('../assets/homeIcon.png')}/>
                </TouchableOpacity>
        
                <TouchableOpacity onPress = { () => props.handlePageChange('Schedule')}>
                <Image style={{width: 50, height: 50}} value="ClockIcon"source={require('../assets/clockIcon4.png')}/>
                </TouchableOpacity>
        
                <TouchableOpacity onPress = { () => props.handlePageChange('SearchJobs')}>
                <Image style={{width: 50, height: 50}} value="SearchIcon"source={require('../assets/searchIcon.png')}/>
                </TouchableOpacity>
        
                <TouchableOpacity onPress = { () => props.handlePageChange('Profile')}>
                <Image style={{width: 50, height: 50}} value="UserIcon"source={require('../assets/userIcon2.png')}/>
                </TouchableOpacity>
              </View>
              
            </View>
            
            )


}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      // marginTop: 20,
      // justifyContent: 'center',
    },
    
    text: {
      color: '#000',
    },
  // ====================
  // ====== Header ======
  // ====================

  header: {
    height: '20%',
    width: '100%',
    backgroundColor: '#82D0F0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'red',
  },


  // ====================
  // ====== Login ======
  // ====================

  Login: {
    backgroundColor: '#E0E1E2',
    height: '80%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
  },
  loginBox: {
    height: '20%',
  },
  input: {
    backgroundColor: 'white',
    height: '50%',
    borderWidth: 1,
    borderColor: 'darkgray',
    margin: 2,
  },
  loginButton: {
    width: '60%',
  },
  
  
  buttomSections:{
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  
  ForgotButton: {
    
  },
  ContactButton: {
    
  },
  
  
  // ====================
  // ====== Login ======
  // ====================

  main: {
    backgroundColor: '#E0E1E2',
    height: '70%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  HomeForm: {
    backgroundColor: 'lightblue',
    width: '80%',
    height: '80%',
    justifyContent: 'space-evenly',
  },
  footer:{
    height: '10%',
    width: '100%',
    backgroundColor: '#82D0F0',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
     
  },
  icon: {
    height: 50,
    width: 50,
    backgroundColor: '#82D0F0',
    color: 'black',
  }
});


export default SearchJobs